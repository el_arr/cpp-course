/*
 * Author: Eldar Mingachev
 */

#include <mpi.h>

using namespace std;

int main(int argc, char **argv) {
    const int M = 4, N = 4;
    int size, rank, i, j, l, *a, *part,
            max_pt, max_, sum_pt;

    MPI_Status status;
    MPI_Init(&argc, &argv);
    MPI_Comm_size(MPI_COMM_WORLD, &size);
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);

    a = new int[M * N];
    if (rank == 0) {
        for (i = 0; i < M; ++i) {
            for (j = 0; j < N; ++j) {
                a[i * M + j] = i * M + j;
            }
        }
    }

    l = N * M / size;
    if (l > 0) {
        part = new int[l];
        MPI_Scatter(a, l, MPI_INT, part, l, MPI_INT, 0, MPI_COMM_WORLD);

        max_pt = *part;
        for (i = 0; i < l / M; ++i) {
            sum_pt = 0;
            for (j = 0; j < N; ++j)
                sum_pt += abs(part[i * l / M + j]);
            max_pt = max(max_pt, sum_pt);
        }

        MPI_Reduce(&max_pt, &max_, 1, MPI_INT, MPI_MAX, 0, MPI_COMM_WORLD);
        if (rank == 0) {
            printf("s = %d\n", max_);
        }
    }

    MPI_Finalize();
}

