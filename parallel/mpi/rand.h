/*
 * Author: Eldar Mingachev
 */

#include <random>

#ifndef MPI_RAND_H
#define MPI_RAND_H

// Generate random integer.
int random_int() {
    std::random_device rd;
    std::uniform_int_distribution<int> uid(-256, 255);
    return uid(rd);
}

typedef unsigned char byte;

byte random_byte() {
    std::random_device rd;
    std::uniform_int_distribution<byte> uid(0, 255);
    return uid(rd);
}

#endif //MPI_RAND_H
