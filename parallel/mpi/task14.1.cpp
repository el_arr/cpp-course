/*
 * Author: Eldar Mingachev
 */

#include <mpi.h>

#include "print.h"

int main(int argc, char **argv) {
    const int M = 4, N = 8;
    int size, rank, i, j, k,
            l = M * N,
            *a, *b, *c;
    MPI_Status status;
    MPI_Init(&argc, &argv);
    MPI_Comm_size(MPI_COMM_WORLD, &size);
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);
    if (size == 3) {
        MPI_Datatype t;
        MPI_Type_vector(M, N, N, MPI_INT, &t);
        MPI_Type_commit(&t);
        if (rank == 0) {
            a = new int[N * N];
            b = new int[l];
            c = new int[l];
            int c_c = 0, c_b = 0;
            for (i = 0; i < N; ++i) {
                for (j = 0; j < N; ++j) {
                    k = i * N + j;
                    if (i % 2 == 0) {
                        c[c_c] = k;
                        ++c_c;
                    } else {
                        b[c_b] = k;
                        ++c_b;
                    }
                    a[i * N + j] = k;
                }
            }
            MPI_Send(b, 1, t, 1, 0, MPI_COMM_WORLD);
            MPI_Send(c, 1, t, 2, 0, MPI_COMM_WORLD);
        } else if (rank == 1) {
            b = new int[l];
            MPI_Recv(b, 1, t, 0, 0, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
            print_matrix(b, M, N, rank);
        } else if (rank == 2) {
            c = new int[l];
            MPI_Recv(c, 1, t, 0, 0, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
            print_matrix(c, M, N, rank);
        }
        MPI_Type_free(&t);
    }
    MPI_Finalize();
}

