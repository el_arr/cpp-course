/*
 * Author: Eldar Mingachev
 */

#include <mpi.h>
#include <chrono>

#include "rand.h"

int main(int argc, char **argv) {
    const int N = 1000;
    byte send[N * N], recv[N * N];
    int size, rank;
    double start = 0;
    const bool PINGPONG = true;

    MPI_Status status;
    MPI_Init(&argc, &argv);
    MPI_Comm_size(MPI_COMM_WORLD, &size);
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);

    if (size != 2) {
        MPI_Abort(MPI_COMM_WORLD, MPI_ERR_COUNT);
    }
    if (rank == 0 || !PINGPONG) {
        for (byte &t : send) t = random_byte();
    }
    MPI_Barrier(MPI_COMM_WORLD);

    for (int i = N; i <= N * N; i += N) {
        MPI_Barrier(MPI_COMM_WORLD);
        if (rank == 0) {
            start = MPI_Wtime();

            MPI_Send(send, i, MPI_UNSIGNED_CHAR, 1, 0, MPI_COMM_WORLD);
            MPI_Recv(recv, i, MPI_UNSIGNED_CHAR, 1, 1, MPI_COMM_WORLD, &status);
        } else {
            if (PINGPONG) {
                MPI_Recv(recv, i, MPI_UNSIGNED_CHAR, 0, 0, MPI_COMM_WORLD, &status);
                MPI_Send(recv, i, MPI_UNSIGNED_CHAR, 0, 1, MPI_COMM_WORLD);
            } else {
                MPI_Request request;
                MPI_Irecv(recv, i, MPI_UNSIGNED_CHAR, 0, 0, MPI_COMM_WORLD, &request);
                MPI_Send(send, i, MPI_UNSIGNED_CHAR, 0, 1, MPI_COMM_WORLD);
                MPI_Wait(&request, &status);
            }
        }

        if (!PINGPONG)
            MPI_Barrier(MPI_COMM_WORLD);
        if (rank == 0 && i % (100 * N) == 0)
            printf("Message length = %d;\tExecution time = %lf\n", i, (MPI_Wtime() - start));
    }

    MPI_Finalize();
}

