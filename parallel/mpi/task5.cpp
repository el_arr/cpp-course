/*
 * Author: Eldar Mingachev
 */

#include <mpi.h>

#include "rand.h"
#include "print.h"

using namespace std;

int main(int argc, char **argv) {
    const int N = 12;
    int i, size, rank, *a,
            block, left;

    MPI_Status status;
    MPI_Init(&argc, &argv);
    MPI_Comm_size(MPI_COMM_WORLD, &size);
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);

    if (rank == 0) {
        a = new int[N];
        for (i = 0; i < N; ++i) {
            a[i] = random_int();
        }

        block = N / size + 1;
        left = N - block;
        for (i = 1; i < size; ++i) {
            MPI_Send(
                    (left > 0) ? a + i * block : a,
                    (left > 0) ? ((left > block) ? block : left) : 0,
                    MPI_INT, i, 0, MPI_COMM_WORLD
            );
            left -= block;
        }

        print_block(a, block, rank);
    } else {
        MPI_Probe(0, 0, MPI_COMM_WORLD, &status);
        MPI_Get_count(&status, MPI_INT, &block);
        a = new int[block];

        MPI_Recv(a, block, MPI_INT, 0, 0, MPI_COMM_WORLD, &status);

        print_block(a, block, rank);
    }
    delete a;

    MPI_Finalize();
}



