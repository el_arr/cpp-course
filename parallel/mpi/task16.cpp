/*
 * Author: Eldar Mingachev
 */

#include <mpi.h>

#include "print.h"
#include "rand.h"

int main(int argc, char **argv) {
    const int N = 8;
    int size, rank, new_rank, i, j, a[N];
    MPI_Status status;
    MPI_Init(&argc, &argv);
    MPI_Comm_size(MPI_COMM_WORLD, &size);
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);

    if (size == 10) {
        // Create group
        MPI_Group world_group;
        // Create group in the communicator
        MPI_Comm_group(MPI_COMM_WORLD, &world_group);
        // Create new communicator
        MPI_Comm groupcomm;
        // Create new group
        MPI_Group group;
        int ranks[] = {8, 3, 9, 1, 6};
        // Include processes in group
        MPI_Group_incl(world_group, 5, ranks, &group);
        // Group registration in
        MPI_Comm_create(MPI_COMM_WORLD, group, &groupcomm);
        if (groupcomm != MPI_COMM_NULL) {
            new_rank = 1;
            MPI_Comm_rank(groupcomm, &new_rank);
            if (new_rank == 0) {
                for (i = 0; i < N; ++i) a[i] = random_int();
            }
            MPI_Bcast(a, N, MPI_INT, 0, groupcomm);
            print_block(a, N, new_rank, rank);
            if (new_rank == 4) {
                MPI_Send(a, N, MPI_INT, 0, 0, MPI_COMM_WORLD);
            }
        }
        if (rank == 0) MPI_Recv(a, N, MPI_INT, ranks[4], 0, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
        if (groupcomm != MPI_COMM_NULL) MPI_Comm_free(&groupcomm);
        MPI_Group_free(&world_group);
        MPI_Group_free(&group);
    }
    MPI_Finalize();
}

