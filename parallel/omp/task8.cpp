/*
 * Author: Eldar Mingachev
 */

#include <omp.h>
#include <ctime>
#include <iostream>
#include <chrono>
#include <sys/time.h>

#include "rand.h"

using namespace std;

#define MICROSECONDS_NOW chrono::duration_cast<chrono::microseconds>(chrono::system_clock::now().time_since_epoch())

int main() {
    const int N = 5;
    int i, j, matrix[N][N], vector[N], result[N];

    for (i = 0; i < N; ++i) {
        for (j = 0; j < N; ++j) {
            matrix[i][j] = random_int();
        }
        vector[i] = random_int();
        result[i] = 0;
    }

    chrono::microseconds before, after;

    before = MICROSECONDS_NOW;
    #pragma omp parallel for schedule(static, N) private(i, j)
    for (i = 0; i < N; ++i) {
        for (j = 0; j < N; ++j) {
            result[i] += matrix[i][j] * vector[j];
        }
    }
    after = MICROSECONDS_NOW;

    printf("Usual 'for' took %ld microseconds.\n", after.count() - before.count());

    before = MICROSECONDS_NOW;
    #pragma omp parallel for private(i, j)
    for (i = 0; i < N; ++i) {
        result[i] = 0;
        for (j = 0; j < N; ++j) {
            result[i] += matrix[i][j] * vector[j];
        }
    }
    after = MICROSECONDS_NOW;

    printf("OMP 'for' took %ld microseconds.\n", after.count() - before.count());

    return 0;
}