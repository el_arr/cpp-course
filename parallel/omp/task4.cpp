/*
 * Author: Eldar Mingachev
 */

#include <omp.h>
#include <iostream>

using namespace std;

int min(const int *array, const size_t size) {
    int min = *array;
    for (size_t i = 1; i < size; ++i) {
        if (*(array + i) < min) {
            min = *(array + i);
        }
    }
    return min;
}

int max(const int *array, const size_t size) {
    int max = *array;
    for (size_t i = 1; i < size; ++i) {
        if (*(array + i) > max) {
            max = *(array + i);
        }
    }
    return max;
}

int main() {
    const int N = 10;
    int a[N] = {12, 13, 14, 15, 16, 17, 18, 19, 20, 11},
            b[N] = {2, 3, 4, 5, 6, 7, 8, 9, 10, 1};

    omp_set_num_threads(2);
    #pragma omp parallel
    {
        if (omp_get_thread_num() == 0) {
            printf("Thread %d / %d: min = %d.\n", omp_get_thread_num(), omp_get_num_threads(), min(a, N));
        } else if (omp_get_thread_num() == 1) {
            printf("Thread %d / %d: max = %d.\n", omp_get_thread_num(), omp_get_num_threads(), max(b, N));
        }
    }

    return 0;
}
