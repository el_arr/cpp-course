/*
 * Author: Eldar Mingachev
 */

#include <omp.h>
#include <iostream>

using namespace std;

int main() {
    int num_threads = 3;
    omp_set_num_threads(num_threads);
    #pragma omp parallel if(num_threads >  1)
    {
        printf("Zone 1: Thread %d / %d\n", omp_get_thread_num(), omp_get_num_threads());
    }

    num_threads = 1;
    omp_set_num_threads(num_threads);
    #pragma omp parallel if(num_threads > 1)
    {
        printf("Zone 2: Thread %d / %d\n", omp_get_thread_num(), omp_get_num_threads());
    }

    return 0;
}
