/*
 * Author: Eldar Mingachev
 */

#include <omp.h>
#include <iostream>
#include <vector>
#include <cmath>

using namespace std;

int main() {
    bool prime;
    int i, j, start, end;
    vector<int> primes = vector<int>();
    cin >> start >> end;

    if (start < 2) {
        start = 2;
    }
    if (end < 2) {
        end = 2;
    }
    if (end < start) {
        i = start;
        start = end;
        end = i;
    }

    omp_set_num_threads(8);

    #pragma omp parallel for private(prime) shared(primes) schedule(dynamic)
    for (i = 2; i <= end; ++i) {
        prime = true;
        #pragma omp critical
        for (j = 0; j < primes.size() && prime; ++j) {
            if (i % primes.at(j) == 0) {
                prime = false;
            }
        }

        if (prime) {
            #pragma omp critical
            primes.push_back(i);
            if (i > start)
                printf("%d  ", i);
        }
    }

    return 0;
}