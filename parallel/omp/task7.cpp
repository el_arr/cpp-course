/*
 * Author: Eldar Mingachev
 */

#include <omp.h>
#include <iostream>

#include "rand.h"

using namespace std;

int main() {
    const int N = 12;
    int i, a[N], b[N], c[N];

    omp_set_num_threads(3);
    #pragma omp parallel for schedule(static, 2) private(i)
    for (i = 0; i < N; ++i) {
        a[i] = random_int();
        b[i] = random_int();
        printf(
                "Zone 1, thread %d / %d: a[%d] = %d, b[%d] = %d.\n",
                omp_get_thread_num(), omp_get_num_threads(), i, a[i], i, b[i]
        );
    }

    omp_set_num_threads(4);
    #pragma omp parallel for schedule(dynamic, 2) private(i)
    for (i = 0; i < 12; ++i) {
        c[i] = a[i] + b[i];
        printf(
                "Zone 2, thread %d / %d: c[%d] = %d.\n",
                omp_get_thread_num(), omp_get_num_threads(), i, c[i]
        );
    }

    return 0;
}

