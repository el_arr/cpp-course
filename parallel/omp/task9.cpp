/*
 * Author: Eldar Mingachev
 */

#include <omp.h>
#include <iostream>
#include <random>

#include "rand.h"

using namespace std;

int main() {
    const int L = 6;
    const int C = 8;
    int i, j, **d = new int *[L];

    for (i = 0; i < L; ++i) {
        *(d + i) = new int[C];
        for (j = 0; j < C; ++j) {
            *(*(d + i) + j) = random_int();
        }
    }

    int min = **d, max = min;

    omp_set_num_threads(4);
    #pragma omp parallel for private (i, j) shared(min, max)
    for (i = 0; i < L; ++i) {
        #pragma omp parallel for private (j) shared(min, max)
        for (j = 0; j < C; ++j) {
            if (d[i][j] < min) {
            #pragma omp critical (minimum)
                if (d[i][j] < min)
                    min = d[i][j];
            }
            if (d[i][j] > max) {
            #pragma omp critical (maximum)
                max = d[i][j];
            }
        }
    }

    printf("Max = %d, Min = %d.", max, min);
}

