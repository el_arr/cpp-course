/*
 * Author: Eldar Mingachev
 */

#include <omp.h>
#include <iostream>
#include <chrono>

using namespace std;

int main() {
    const int N = 30;
    int a[N] = {1, 0, 1, 1, 0, 1, 0, 0, 0, 1, 1, 1, 0, 1, 0, 1, 0, 0, 0, 1, 0, 1, 1, 1, 0, 0, 0, 1, 0, 1},
            i, j, p = 1, result = 0;

    #pragma omp parallel for private(i) shared(p) schedule(dynamic) ordered
    for (i = N - 1; i >= 0; --i) {
        p *= (i == N - 1) ? 1 : 2;
    #pragma omp atomic
        result += a[i] * p;
    }
    printf("Result is %d.\n", result);

    return 0;
}