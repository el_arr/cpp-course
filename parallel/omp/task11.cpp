/*
 * Author: Eldar Mingachev
 */

#include <omp.h>
#include <iostream>

#include "rand.h"

using namespace std;

int main() {
    const int N = 20;
    int i, max, a[N];

    for (i = 0; i < N; ++i) {
        a[i] = random_int();
    }
    max = *a;

    omp_set_num_threads(4);
    #pragma omp parallel for schedule(dynamic, N/4) private(i) shared(max)
    for (i = 0; i < N; ++i) {
        #pragma omp critical
        if ((a[i] % 7 == 0) && (a[i] > max)) {
            max = a[i];
        }
    }

    printf("Max div by 7 = %d.", max);

    return 0;
}