/*
 * Author: Eldar Mingachev
 */

#include <omp.h>
#include <iostream>
#include <random>

#include "rand.h"

using namespace std;

int main() {
    const int N = 30;
    int i, count = 0, a[N];

    for (i = 0; i < N; ++i) {
        *(a + i) = random_int();
    }

    omp_set_num_threads(4);
    #pragma omp parallel for private(i)
    for (i = 0; i < N; ++i) {
        if (a[i] % 9 == 0) {
        #pragma omp atomic
            count++;
        }
    }

    printf("Div9 count = %d", count);
}