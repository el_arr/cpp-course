#include <iostream>

using namespace std;

char *resize(const char *str, unsigned size, unsigned new_size) {
    char *c = new char[new_size];
    for (int i = 0; i < (size > new_size ? new_size : size); ++i) {
        c[i] = str[i];
    }
    delete[] str;
    return c;
}

int main() {
    char *str = new char[12];
    for (int i = 0; i < 12; i++) {
        str[i] = (char) ('a' + i);
    }
    cout << str << endl;
    char *p = resize(str, 12, 9);
    cout << p;
    return 0;
}