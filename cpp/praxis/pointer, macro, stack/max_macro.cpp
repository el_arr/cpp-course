#include <iostream>
using namespace std;

#define MAX(a, b, c) do {int aa = (a); int bb = (b); (c) = (aa > bb ? aa : bb);} while (false);

int main() {
	int a = 10;
	int b = 20;
	int c = 0;
	MAX(a, b, c);
	cout << "c = " << c << endl;
	MAX(a += b, b, c);
	cout << "c = " << c << endl;
	return 0;
}