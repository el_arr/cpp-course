#include <iostream>

using namespace std;

void swap(int &a, int &b) {
    int t = b;
    b = a;
    a = t;
}

void sort_1(int *a, int *b, int *c) {
    int t;
    if (a > b) {
        if (a > c) {
            if (b > c)
                cout << "a, b, c" << endl;
            else 
                cout << "a, c, b" << endl;
        } else
            cout << "c, a, b" << endl;
    } else {
        if (b > c) {
            if (c > a)
                cout << "b, c, a" << endl;
            else
                cout << "b, a, c" << endl;
        } else
            cout << "c, b, a" << endl;
    }
}

void sort_2(int &a, int &b, int &c) {
    if (a > b) {
        if (a > c) {
            if (b > c)
                cout << "a, b, c" << endl;
            else {
                cout << "a, c, b" << endl;
                swap(b, c);
            }
        } else {
            cout << "c, a, b" << endl;
            swap(a, c);
            swap(c, b);
        }
    } else {
        if (b > c) {
            if (c > a) {
                cout << "b, c, a" << endl;
                swap(a, b);
                swap(b, c);
            } else {
                cout << "b, a, c" << endl;
                swap(a, b);
            }
        } else {
            cout << "c, b, a" << endl;
            swap(a, c);
        }
    }
}

int main() {
    int a = 10, b = 11, c = 9;
    cout << a << " " << b << " " << c << endl;
    /* sort_1(&a, &b, &c); */
    sort_2(a, b, c);
    cout << a << " " << b << " " << c << endl;
}