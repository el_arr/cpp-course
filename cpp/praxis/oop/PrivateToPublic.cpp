#include <iostream>

struct Cls {
private:
    char c;
    double d;
    int i;
};

struct New {
    char c;
    double d;
    int i;
};

char &get_c(Cls &cls) {
    auto *n = (New *) (&cls);
    return n->c;
}

int main() {
    return 0;
}