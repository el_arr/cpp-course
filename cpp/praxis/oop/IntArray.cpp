#include <iostream>

struct IntArray {
private:
    size_t size;
    int *data;

public:
    explicit IntArray(size_t size) : data(new int[size]), size(size) {};

    IntArray(const int *data, size_t size) {
        this->data = new int[size];
        this->size = size;
        for (int i = 0; i < size; ++i) {
            this->data[i] = data[i];
        }
    }

    //конструктор копирования
    IntArray(IntArray const &a) : size(a.size), data(new int[size]) {
        for (size_t i = 0; i < size; i++) {
            data[i] = a.data[i];
        }
    };

    //конструктор присваивания
    IntArray &operator=(IntArray const &a) {
        if (this != &a) {
            delete[] data;
            size = a.size;
            data = new int[size];
            for (int i = 0; i < size; ++i) {
                data[i] = a.data[i];
            }
            /*
             * or with "#include <algorithm>"
             * IntArray(a).swap(*this);
             */
        }
        return *this;
    }

    ~IntArray() {
        delete[] data;
    }

    void resize(size_t newsize) {
        IntArray t(newsize);
        size_t n = newsize > size ? size : newsize;
        for (size_t i = 0; i < n; ++i) {
            t.data[i] = data[i];
        }
        swap(t);
    }

    void swap(IntArray &a) {
        size_t temp_size = this->size;
        int *temp_data = this->data;
        this->size = a.size;
        this->data = a.data;
        a.data = temp_data;
        a.size = temp_size;
    }

    size_t getSize() const {
        return size;
    }

    int *getData() const {
        return data;
    }

    int getItem(size_t i) const {
        return data[i];
    }

    int &getItem(size_t i) {
        return data[i];
    }

    void setItem(int n, size_t i) {
        data[i] = n;
    }
};

int main() {
    int a1[3] = {1, 2, 3};
    int a2[4] = {4, 5, 6, 7};
    auto *ia1 = new IntArray(&a1[0], 3);
    auto *ia2 = new IntArray(&a2[0], 4);
    ia1->swap(*ia2);
    std::cout << "IntArray 1" << std::endl;
    for (size_t i = 0; i < ia1->getSize(); ++i) {
        std::cout << ia1->getItem(i) << std::endl;
    }
    std::cout << "IntArray 2" << std::endl;
    for (size_t i = 0; i < ia2->getSize(); ++i) {
        std::cout << ia2->getItem(i) << std::endl;
    }
    return 0;
}