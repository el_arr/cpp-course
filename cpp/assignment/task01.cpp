/** Author: Mingachev Eldar **/

/**
 * Потоки разделяют между собой List<CBlock>,
 * где CBlock это структура содержащая в себе поле данные (data) типа byte (размер 512 KB), и поле hash.
 * 1. Первые 2 потока производят случайный (random) набор байт,
 * создают соответствующую структуру CBlock и ставят в конец List.
 * 2. Два следующих потока производят обработку очереди,
 * поочередно считая хещ функцию SHA-256 для блока данных, записывая результат в поле hash.
 * - Приложение консольное, выводит текущее состояние очереди (в очереди/завершено/процент завершения).
 * - После того как количество блоков становится равно 512, мы останавливаем producer'ов и ждем окончания обработки.
 * После окончания работы, все хэшы последовательно записываются в бинарный файл.
 **/

#include <iostream>
#include <fstream>
#include <thread>
#include <mutex>
#include <list>
#include <string>
#include <random>

#include "picosha2.h"

using namespace std;

typedef unsigned char byte;

const int D_SIZE = 512 * 1024, // Data array size.
        B_SIZE = 512; // Overall amount of CBlocks.
int processed = 0, // Amount of processed CBlocks.
        added = 0; // Amount of added CBlocks.

struct CBlock {
private:
    byte data[D_SIZE] = {};
    string hash;

public:
    CBlock() = default;

    explicit CBlock(byte *data) {
        copy(data, data + D_SIZE, this->data);
    }

    const byte *getData() const {
        return data;
    }

    const string &getHash() const {
        return hash;
    }

    void setHash(const string &hash) {
        CBlock::hash = hash;
    }
};

mutex list_mutex;
list<CBlock> src; // Shared source list.
list<CBlock> out; // List for hash output to file.

// Generate random byte.
byte random_byte() {
    random_device rd;
    uniform_int_distribution<byte> uid(0, 255);
    return uid(rd);
}

// Generate new array of D_SIZE (512 * 1024) random bytes.
CBlock new_data() {
    byte data[D_SIZE];
    for (byte &i : data) i = random_byte();
    return CBlock(data);
}

// Add generated array to the list.
void add_data() {
    lock_guard<mutex> lock(list_mutex);
    src.push_back(new_data());
    if (added < B_SIZE) {
        added++;
    }
}

// Looping wrapper method for "add_string" method with console output.
void data_handler() {
    while (added < B_SIZE) {
        add_data();
        cout << "In queue: " << added - processed
             << ". Ready: " << processed
             << ". Done: " << 100.0 * processed / B_SIZE << " percent" << endl;
    }
}

// Conversion from byte array to byte vector.
vector<byte> data_vector(const byte *data) {
    return vector<byte>(data, data + D_SIZE);
}

// Hash generation function. Using SHA256 by PicoSHA2.
string new_hash(const vector<byte> &src) {
    vector<unsigned char> hash(picosha2::k_digest_size);
    picosha2::hash256(src, hash);
    return picosha2::bytes_to_hex_string(hash);
}

// Get CBlock data and add generated hash to CBlock.
void add_hash() {
    lock_guard<mutex> lock(list_mutex);
    if (src.empty()) {
        this_thread::sleep_for(
                chrono::milliseconds(10)
        );
        return;
    }
    CBlock front = src.front();
    front.setHash(new_hash(
            data_vector(front.getData())
    ));
    src.pop_front();
    out.push_back(front);
    if (processed < B_SIZE) {
        processed++;
    }
}

// Looping wrapper method for "add_hash" method with console output.
void hash_handler() {
    while (processed < B_SIZE) {
        add_hash();
        cout << "In queue: " << added - processed
             << ". Ready: " << processed
             << ". Done: " << 100.0 * processed / B_SIZE << " percent" << endl;
    }
}

// Write generated hashes to the log file.
void write() {
    ofstream output("result.bin", ios::out | ios::binary | ios::app);
    char *hash;
    for (auto &block: out) {
        hash = (char *) block.getHash().c_str();
        output << hash << endl;
    }
    output.close();
}

int main() {
    thread threads[4] = {
            thread(data_handler), thread(data_handler),
            thread(hash_handler), thread(hash_handler)
    };
    for (auto &thread : threads) {
        thread.join();
    }
    write();
    return 0;
}